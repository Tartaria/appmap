# AppMap

AppMap:
- Watch Some Videos => Kodi
- Visit the Radio Chat => Matrix.org, Rocket
- Check Out Our Code => Gitlab
- LEARN => Drupal Education
- Explore the Foundry => NextCloud
- See the Starforts => UniGraph GIS (Geographic Information System)
- Discover Tartaria => Wiki
- Tartary Education => Drupal Education distro
- Open Educational Resources => Stor:K, HistoryGraph, ScienceGraph
- Contribute => Dash

http://tv.tartarynova.com/app/